'''
Autor: Robert Mielewczyk
Zadanie: Zadanie polega na pobraniu (web scraping) wierszów wybranego autora (Teofil Lenartowicz)
i dokonaniu analizy ilości słów napisanych przez tego autora
'''

from bs4 import BeautifulSoup
from constants import LINKS, MAIN_URL
import requests


def get_poems(main_url: str, url_endings: str) -> list:
    found_poems = []
    for url_ending in url_endings:
        html = BeautifulSoup(requests.get(main_url + url_ending).text)
        poem = html.find("div", {"class": "poem"}).p.get_text()
        found_poems.append(poem)
    return found_poems

def count_words_in_poems(poems: list) -> int:
    return sum([len(poem.split(' ')) for poem in poems])


if __name__ == '__main__':
    poems = get_poems(MAIN_URL, LINKS)
    total_number_of_words_written_by_author = count_words_in_poems(poems)
    print(f"Teofil Lenartowicz napisał w swoim życiu: {total_number_of_words_written_by_author} słów")

